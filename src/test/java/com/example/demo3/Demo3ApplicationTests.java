package com.example.demo3;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.demo3.domain.Prices;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDateTime;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class Demo3ApplicationTests {

    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void test1() {
        int brandId = 1;
        int productId = 35457;
       // LocalDateTime dateTime = LocalDateTime.of(2023, 06, 14, 10, 0, 0);

        String url = "http://localhost:" + port + "/products/{brandId}/{productId}";

        ResponseEntity<Prices> response = restTemplate.getForEntity(url, Prices.class, brandId, productId);
        Prices prices = response.getBody();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(prices);
        assertEquals(brandId, prices.getBRAND_ID());
        assertEquals(productId, prices.getPRODUCT_ID());
    }
    
}




