package com.example.demo3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//enrutamiento

import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//
@SpringBootApplication
public class Demo3Application implements WebMvcConfigurer{

	public static void main(String[] args) {
		SpringApplication.run(Demo3Application.class, args);
	}

}
