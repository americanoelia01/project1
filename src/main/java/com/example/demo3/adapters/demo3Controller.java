package com.example.demo3.adapters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo3.domain.Prices;
import com.example.demo3.domain.User;
import com.example.demo3.infraestructure.PriceRepository;
import com.example.demo3.infraestructure.UserRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Optional;

import javax.swing.text.View;

@Controller
public class demo3Controller {

    
    @Autowired
    private UserRepository repo;

    @Autowired
    private PriceRepository repo2;

    @GetMapping("/createUsers")
    public String users(){
    LocalDateTime dateActual =LocalDateTime.now();
    User user1=new User();
    user1.setName("USER2");
    user1.setLAST_UPDATE(dateActual);
    repo.save(user1);

    LocalDateTime dateActual2 =LocalDateTime.now();
    User user2=new User();
    user1.setName("USER1");
    user1.setLAST_UPDATE(dateActual2);
    repo.save(user2);

    return "createUsers";

}   
    @GetMapping("/createPrices")
    public String createPrices(){
    BigDecimal numero1 = new BigDecimal("35.5");
    LocalDateTime endDate =LocalDateTime.of(2020,06,14,00,00,00);
    LocalDateTime StartDate =LocalDateTime.of(2020,12,31,23,59,59);
    Prices price1=new Prices();
    price1.setPRODUCT_ID(35455);    
    price1.setBRAND_ID(1);
    price1.setCURR("EUR");
    price1.setPRICE_LIST(1);
    price1.setPRIORITY(0);
    price1.setPRICE(numero1);
    price1.setEND_DATE(endDate);
    price1.setSTART_DATE(StartDate);
    repo2.save(price1);



    BigDecimal numero2 = new BigDecimal("25.45");
    LocalDateTime endDate2 =LocalDateTime.of(2020,06,14,15,00,00);
    LocalDateTime StartDate2 =LocalDateTime.of(2020,06,14,18,30,00);
    Prices price2=new Prices();
    price2.setPRODUCT_ID(35456);    
    price2.setBRAND_ID(1);
    price2.setCURR("EUR");
    price2.setPRICE_LIST(2);
    price2.setPRIORITY(1);
    price2.setPRICE(numero2);
    price2.setEND_DATE(endDate2);
    price2.setSTART_DATE(StartDate2);
    repo2.save(price2);

    
    BigDecimal numero3 = new BigDecimal("31.5");
    LocalDateTime endDate3 =LocalDateTime.of(2020,06,15,00,00,00);
    LocalDateTime StartDate3 =LocalDateTime.of(2020,06,15,11,00,00);
    Prices price3=new Prices();
    price3.setPRODUCT_ID(35457);    
    price3.setBRAND_ID(1);
    price3.setCURR("EUR");
    price3.setPRICE_LIST(3);
    price3.setPRIORITY(1);
    price3.setPRICE(numero3);
    price3.setEND_DATE(endDate3);
    price3.setSTART_DATE(StartDate3);
    repo2.save(price3);

    BigDecimal numero4 = new BigDecimal("38.95");
    LocalDateTime endDate4 =LocalDateTime.of(2020,06,15,16,00,00);
    LocalDateTime StartDate4 =LocalDateTime.of(2020,12,31,10,59,59);
    Prices price4=new Prices();
    price4.setPRODUCT_ID(35458);    
    price4.setBRAND_ID(1);
    price4.setCURR("EUR");
    price4.setPRICE_LIST(4);
    price4.setPRIORITY(1);
    price4.setPRICE(numero4);
    price4.setEND_DATE(endDate4);
    price4.setSTART_DATE(StartDate4);
    repo2.save(price4);

    return "createPrices";

    } 


    @GetMapping("/returnData")
    public String returnData(Model model){
    List<Prices> pricesList = repo2.findAll(); // Fetch all prices from the repository
    model.addAttribute("pricesList", pricesList); // Add the pricesList to the model

    return "returnData";


    } 



    @Autowired
    private CrudRepository<Prices, Integer> priceRepository;
    


    
       
    @GetMapping("/searchData/{id}/{brand_id}")
    public String findById(@PathVariable("id") Integer id, Integer brand_id, Model model) {
        Optional<Prices> prices = priceRepository.findById(id);
        if (prices.isPresent()) {
            Prices pricesData = prices.get();
            model.addAttribute("prices", pricesData);
            return "result"; // Nombre de la vista que devuelve si encuentra el id
        } else {
            return "errorView"; // Nombre de la vista en caso de error
        }
    }

    /**
     * 
    //introduciendo el id devuelve el producto con los datos correspondientes
    //devuelve una respuesta HTTP con el objeto Prices
     * 
     *     @GetMapping("/searchData/{id}")
    public ResponseEntity<Prices> findById(@PathVariable("id") Integer id) {
        Optional<Prices> prices = priceRepository.findById(id);
        return prices.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    } 

     */


}



