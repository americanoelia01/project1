package com.example.demo3.infraestructure;

import org.springframework.data.jpa.repository.JpaRepository;


import com.example.demo3.domain.User;

public interface UserRepository extends JpaRepository<User, String>{
   // User findByUsername(String username);
}
