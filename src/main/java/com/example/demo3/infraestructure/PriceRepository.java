package com.example.demo3.infraestructure;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo3.domain.Prices;

public interface PriceRepository extends JpaRepository<Prices, Integer>{


}
