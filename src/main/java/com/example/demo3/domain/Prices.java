package com.example.demo3.domain;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
//import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@Entity(name="Price") //nombre de mi entidad
@Table(name="PRICES") //nombre de la tabla
@Getter
@Setter

public class Prices {
    
    private int BRAND_ID;
        
    private LocalDateTime START_DATE;
    
    private LocalDateTime END_DATE;
    
    private int PRICE_LIST;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int PRODUCT_ID;
    
    private int PRIORITY;
    
    private BigDecimal PRICE; 
    
    private String CURR;
}
