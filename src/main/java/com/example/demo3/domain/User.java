package com.example.demo3.domain;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
//import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity(name="User") //nombre de mi entidad
@Table(name="USERS")  //nombre de mi tabla
//@Builder //constructor
@Getter
@Setter

public class User {
    
    @Id
    private String name;    
    private LocalDateTime LAST_UPDATE;
    public User(String name , LocalDateTime LAST_UPDATE){
        this.name=name;
        this.LAST_UPDATE=LAST_UPDATE;
    }

    public User() {
        // Constructor sin argumentos
    }
}
