This practice must be solved following the hexagonal architecture approach, applying TDD to drive the code and DDD to model the domain.
The infrastructure will be built using the Spring Boot framework.
We will be using PostgreSQL as the database.

The project will be divided into three parts:
-Domain
-Infrastructure
-Adapters

To create the users and the tables we will execute the route:
http://localhost:8080/createPrices
http://localhost:8080/createUsers

The route return the data in a table of Prices:
http://localhost:8080/returnData 

The route return the data with the PRODUCT_ID with the id set of Prices:
http://localhost:8080/searchData/{id}
